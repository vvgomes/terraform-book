provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket  = "terraform-up-and-running-state-vvgomes"
    key     = "prod/services/webserver-cluster/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true    
  }
}

module "webserver_cluster" {
  source = "git::https://gitlab.com/vvgomes/terraform-book-modules.git//services/webserver-cluster?ref=v0.1.3"

  cluster_name = "webservers-prod"
  db_remote_state_bucket = "terraform-up-and-running-state-vvgomes"
  db_remote_state_key = "prod/data-stores/mysql/terraform.tfstate"

  instance_type = "t2.micro"
  min_size = 3
  max_size = 4
  enable_autoscaling = true

  enable_new_user_data = false
}
