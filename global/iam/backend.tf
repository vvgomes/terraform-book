terraform {
  backend "s3" {
    bucket  = "terraform-up-and-running-state-vvgomes"
    key     = "iam/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true    
  }
}
