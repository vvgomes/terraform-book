provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-up-and-running-state-vvgomes"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "dynamodb-terraform-lock" {
   name = "terraform-lock"
   hash_key = "LockID"
   read_capacity = 20
   write_capacity = 20

   attribute {
      name = "LockID"
      type = "S"
   }

   tags {
     Name = "Terraform Lock Table"
   }
}

terraform {
  backend "s3" {
    bucket  = "terraform-up-and-running-state-vvgomes"
    key     = "terraform.tfstate"
    region  = "us-east-1"
    encrypt = true    
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"
  config {
    bucket  = "terraform-up-and-running-state-vvgomes"
    key     = "terraform.tfstate"
    region  = "us-east-1"
  }
}
