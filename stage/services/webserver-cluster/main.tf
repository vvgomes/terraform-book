provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket  = "terraform-up-and-running-state-vvgomes"
    key     = "stage/services/webserver-cluster/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true    
  }
}

module "webserver_cluster" {
  source = "git::https://gitlab.com/vvgomes/terraform-book-modules.git//services/webserver-cluster?ref=v0.2.0"

  ami = "ami-40d28157"
  server_text = "Foo Bar"

  cluster_name = "webservers-stage"
  db_remote_state_bucket = "terraform-up-and-running-state-vvgomes"
  db_remote_state_key = "stage/data-stores/mysql/terraform.tfstate"

  instance_type = "t2.nano"
  min_size = 2
  max_size = 2
  enable_autoscaling = false
}

resource "aws_security_group_rule" "allow_testing_inbound" {
  type = "ingress"
  security_group_id = "${module.webserver_cluster.elb_security_group_id}"

  from_port = 12345
  to_port = 12345
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}
